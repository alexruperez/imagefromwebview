
# ImageFromWebView


## Overview

Captures an image of a UIWebView, makes an UIImage, shows on an UIImageView and saves it into the gallery.

# Etc.

* Contributions are very welcome.
* Attribution is appreciated (let's spread the word!), but not mandatory.

## Use it? Love/hate it?

Tweet the author @alexruperez, and check out alexruperez's blog: http://alexruperez.com
